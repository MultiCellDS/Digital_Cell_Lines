Digital cell line for Eu-myc/Arf-/- cells, generated from Frieboes et al.. These are Lymphoma drug-sensitive cells.

Created by Edwin Juarez <juarezro[at]usc[dot]edu>

Last updated [2015-08-12T15:17:40-07:00]